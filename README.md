HOW TO RUN
1. Download files to your htdocs directory
2. run 'composer update' command for PHPUnit download
3. run 'composer dump-autoload'
4. run tests using phpunit.xml configuration file or by hand using your IDE
Example: 'php vendor/phpunit/phpunit/phpunit --configuration phpunit.xml'

OPIS:

Celem jest implementacja funkcji "change directory" (cd) w klasie Path reprezentującej abstrakcyjny system plików.

Założenia:

- separatorem ścieżki jest '/'
- ścieżka zawsze rozpoczyna się od '/'
- katalog nadrzędny (parent) dostępny jest przez '..'
- funkcja 'cd' powinna obsługiwać zarówno ścieżki względne (relative) jak i bezwzględne (absolute)
- w implementacji nie używamy żadnych wbudowanych funkcji dedykowanych do obsługi ścieżek ani żadnych dodatkowych bibliotek
- test powinien działać w wersji PHP 7.2.13

Przykład:

$path = new Path('/a/b/c/d');
$path->cd('../x')
echo $path->currentPath;  

Powinno wyświetlić: '/a/b/c/x'

Inne przykładowe wywołania, które powinny być odpowiednio obsłużone przy założeniu, że zawsze inicjujemy obiekt klasy Path w ten sam sposób, tzn  $path = new Path('/a/b/c/d')

cd('..') //zmienia ścieżkę na '/a/b/c'
cd('../..') //zmienia ścieżkę na '/a/b'
cd('x/y/z) //zmienia ścieżkę na /a/b/c/d/x/y/z
cd('/x/y/z') //zmienia ścieżkę na '/x/y/z'
cd('/x/y/z/..') //zmienia ścieżkę na /x/y