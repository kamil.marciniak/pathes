<?php
/**
 * Created by PhpStorm.
 * User: MarciniakK
 * Date: 21/03/2019
 * Time: 22:17
 */

use \Pathes\Path;
use \PHPUnit\Framework\TestCase;

class PathTest extends TestCase
{

    public function testInitialPathIsReturned()
    {
        $path = new Path('/a/b/c/d');
        $this->assertEquals('/a/b/c/d', $path->currentPath);
    }

    //cd('/x/y/z') //zmienia ścieżkę na '/x/y/z'
    public function testAbsolutePathIsReturned()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('/x/y/z');
        $this->assertEquals('/x/y/z', $path->currentPath);
    }

    //cd('..') //zmienia ścieżkę na '/a/b/c'
    public function testUpToParentDirectoryCommand()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('..');
        $this->assertEquals('/a/b/c', $path->currentPath);
    }

    //cd('../..') //zmienia ścieżkę na '/a/b'
    public function testUpToParentsParentDirectoryCommand()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('../..');
        $this->assertEquals('/a/b', $path->currentPath);
    }

    //cd('x/y/z) //zmienia ścieżkę na /a/b/c/d/x/y/z
    public function testRelativePathCommand()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('x/y/z');
        $this->assertEquals('/a/b/c/d/x/y/z', $path->currentPath);
    }

    //cd('/x/y/z/..') //zmienia ścieżkę na /x/y
    public function testAbsolutePathWithUpToParentDirectoryCommand()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('/x/y/z/..');
        $this->assertEquals('/x/y', $path->currentPath);
    }

    //cd('../x') //zmienia ścieżkę na /a/b/c/x
    public function testRelativePathWithUpToParentDirectoryCommand()
    {
        $path = new Path('/a/b/c/d');
        $path->cd('../x');
        $this->assertEquals('/a/b/c/x', $path->currentPath);
    }

}
