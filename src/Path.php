<?php

namespace Pathes;

class Path
{
    public $currentPath;

    function __construct(string $path)
    {
        $this->currentPath = $path;
    }

    public function cd(string $newPath) : void
    {
        $newPathDirectories = explode('/', $newPath);
        $oldPathDirectories = explode('/', $this->currentPath);
        $pathIsAbsolute = empty($newPathDirectories[0]);

        if ($pathIsAbsolute) {
            $oldPathDirectories = [];
        }

        foreach ($newPathDirectories as $newDirectory) {
            if ($newDirectory == '..') {
                array_pop($oldPathDirectories);
            } else {
                $oldPathDirectories[] = $newDirectory;
            }
        }

        $this->currentPath = implode('/', $oldPathDirectories);
    }
}